///Theory

// 1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
// Рекурсия это прием с помощью которого можно разделить одну задачу на несколько мелких.
// Как пример, это когда функция вызывает сама себя. Это как раз и называется рекурсией.


/// Practice


let userNumber = Number(prompt("Please type number", ""));


function factorial(userNumber) {
return (userNumber !== 1) ? userNumber * factorial(userNumber - 1) : 1;
}

function oneMore() {
    while (isNaN(userNumber) || userNumber == "") {
        userNumber = Number(prompt("Please type number", userNumber+""));
    }
    console.log(factorial(userNumber));
}
oneMore()